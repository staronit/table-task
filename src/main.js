import ModalPlugin from './FormTablePlugins/ModalPlugin';
import Mediator from './Mediator/Mediator';
import FormTable from './FormTable';
import CheckboxButtonPlugin from './FormTablePlugins/CheckboxButtonPlugin';
import RowHoverPlugin from './FormTablePlugins/RowHoverPlugin';
import TableSimplicationPlugin from './FormTablePlugins/TableSimplicationPlugin';

window.addEventListener('load', () => {
    const mediator = new Mediator();
    const formElement = document.querySelector('form.content-form');
    const formTable = new FormTable(
        formElement,
        mediator
    );
    
    const overlay = document.querySelector('#overlay');
    formTable.addPlugin(new ModalPlugin(
        document.querySelector('body')
    ));

    formTable.addPlugin(new CheckboxButtonPlugin(
        formElement.querySelector('.content-tools button'),
        formElement.querySelectorAll('td input[type=checkbox]'),
    ));

    formTable.addPlugin(new RowHoverPlugin(
        formElement.querySelectorAll('td input[type=checkbox]'),
        (element) => {
            return element.closest('tr');
        }
    ));

    formTable.addPlugin(new TableSimplicationPlugin(
        formElement.querySelector('table'),
        'hideable',
        'title'
    ));
});
