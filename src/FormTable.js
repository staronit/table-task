import Plugin from './FormTablePlugins/Plugin';
import Mediator from './Mediator/Mediator';

export default class FormTable 
{
    /**
     * @param {HTMLElement} element 
     * @param {Mediator} mediator 
     */
    constructor(element, mediator)
    {
        this._mediator = mediator;
        this._plugins = [];
        this._element = element;
    }

    /**
     * @param {Plugin} plugin 
     */
    addPlugin(plugin)
    {
        plugin.setMediator(this._mediator);
        this._plugins.push(plugin);
        plugin.init();
    }

    /**
     * @return {Array<Plugin>}
     */
    getPlugins()
    {
        return this._plugins;
    }
}