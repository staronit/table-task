export default class Mediator
{
    constructor()
    {
        this._channels = {};
    }

    listen(channel, callback)
    {
        if (!(channel in this._channels)) {
            this._channels[channel] = [];   
        }

        this._channels[channel].push(callback);
    }

    brodcast(channel, data)
    {
        if (!(channel in this._channels)) {
            return;
        }

        this._channels[channel].forEach((callback) => {
            callback(data);
        });
    }
}