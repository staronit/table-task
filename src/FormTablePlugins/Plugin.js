import Mediator from '../Mediator/Mediator';

export default class Plugin
{
    /**
     * @param {Mediator} mediator 
     */
    setMediator(mediator)
    {
        this._mediator = mediator;
    }

    init()
    {

    }

    /**
     * @param {String} channel 
     * @param {Function} callback 
     */
    listen(channel, callback)
    {
        if (typeof this._mediator !== 'undefined') {
            this._mediator.listen(channel, callback);
            return;
        }

        throw new Error('Mediator is not defined');
    }

    /**
     * @param {String} channel 
     * @param {String} method 
     * @param {Any} data 
     */
    brodcast(channel, data)
    {
        if (typeof this._mediator !== 'undefined') {
            this._mediator.brodcast(channel, data);
            return;
        } 

        throw new Error('Mediator is not defined');
    }
}