import Plugin from './Plugin';

const ACTIVE_CLASS = 'active';
const MODAL_CLOSE_BUTTON = 'button';

const OVERLAY_ID = 'overlay';
const MODAL_CLASS = 'modal';
const MODAL_CONTENT_CLASS = 'modal-content';
const BUTTON_TEXT = 'Close';

export default class ModalPlugin extends Plugin
{
    constructor(body)
    {
        super();
        this._body = body;
    }

    init()
    {
        this._initCreateElements();
        this._initStartListening();
    }

    _initCreateElements()
    {
        this._overlayElement = document.createElement('div');
        this._overlayElement.setAttribute('id', OVERLAY_ID);

        this._modalElement = document.createElement('div');
        this._modalElement.classList.add(MODAL_CLASS);

        this._modalContentElement = document.createElement('div');
        this._modalContentElement.classList.add(MODAL_CONTENT_CLASS);

        this._closeButtonElement = document.createElement('button');
        this._closeButtonElement.innerHTML = BUTTON_TEXT;
        this._closeButtonElement.addEventListener('click', () => this._clickClose());

        this._modalElement.appendChild(this._modalContentElement);
        this._modalElement.appendChild(this._closeButtonElement);
        this._overlayElement.appendChild(this._modalElement);

        this._body.appendChild(this._overlayElement);
    }

    _initStartListening()
    {
        this.listen('modal:show', (data) => this._onShow(data));
    }

    _clickClose()
    {
        this._hide();
    }

    /**
     * @param {HTMLElement} content 
     */
    _onShow(content) 
    {
        this._modalContentElement.innerHTML = '';
        this._modalContentElement.appendChild(content);
        this._show();
    }

    _show()
    {
        this._overlayElement.classList.add(ACTIVE_CLASS);
        this._center();
    }

    _hide()
    {
        this._overlayElement.classList.remove(ACTIVE_CLASS);
    }

    _center()
    {
        const windowWidth = window.innerWidth,
            modalWith = this._modalElement.offsetWidth;
            
        this._modalElement.style.left = `${(windowWidth / 2) - (modalWith) / 2}px`;
    }
}