import Plugin from './Plugin';

const CLASS_MODAL = 'modal';
const OVERLAY_ELEMENT = 'div';
const HIDDEN_CLASS = 'hidden';
const DEFAULT_DETAIL_SELECTOR = '.details';
const DEFAULT_HREF_SELECTOR = 'a';
const DETAIL_CONTAINER = 'div';
const DETAIL_CONTAINER_ELEMENT = 'p';

export default class TableSimplicationPlugin extends Plugin
{
    constructor(table, hideableClass, titleClass, detailSelector = DEFAULT_DETAIL_SELECTOR, detailHrefSelector = DEFAULT_HREF_SELECTOR)
    {
        super();
        this._table = table;
        this._hideableClass = hideableClass;
        this._titleClass = titleClass;
        this._detailsElement = table.querySelectorAll(detailSelector);
        this._detailHrefSelector = detailHrefSelector;

        this._hiddenColumnIndexes = [];
        this._titleColumnIndex = 0;
    }

    init()
    {
        this._hideColumns();
        this._initDetails();
    }

    _hideColumns()
    {
        this._table.querySelector('tr').querySelectorAll('th').forEach((column, index) => this._hideColumn(column, index));
    }

    _hideColumn(column, index)
    {
        if (column.classList.contains(this._hideableClass)) {
            this._hideColumnsAtIndex(index + 1);
        }

        if (column.classList.contains(this._titleClass)) {
            this._titleColumnIndex = index + 1;
        }
    }

    /**
     * @param {number} index 
     */
    _hideColumnsAtIndex(index)
    {
        this._table.querySelectorAll(`td:nth-child(${index}),th:nth-child(${index})`).forEach((cell) => this._hideCell(cell));
        this._hiddenColumnIndexes.push(index);
    }

    /**
     * @param {HTMLTableCellElement} cell 
     */
    _hideCell(cell)
    {
        cell.classList.add(HIDDEN_CLASS);
    }

    _initDetails()
    {
        this._detailsElement.forEach((detailElement) => this._initDetail(detailElement));
    }

    /**
     * @param {HTMLElement} detailElement 
     */
    _initDetail(detailElement)
    {
        detailElement.classList.remove(HIDDEN_CLASS);
        const href = detailElement.querySelector(this._detailHrefSelector);
        if (href === null) {
            return;
        }
        href.addEventListener('click', (event) => {
            event.preventDefault();
            return this._detailsClick(event.target);
        });
    }

    /**
     * @param {HTMLElement} hrefElement 
     */
    _detailsClick(hrefElement)
    {
        const tableRow = hrefElement.closest('tr');
        this.brodcast('modal:show', this._createDetailsElement(tableRow));
        return false;
    }

    /**
     * @param {HTMLTableRowElement} tableRow 
     * @return {HTMLElement}
     */
    _createDetailsElement(tableRow)
    {
        const element = document.createElement(DETAIL_CONTAINER);
        element.appendChild(this._createTitleElement(tableRow));
        this._hiddenColumnIndexes.forEach((index) => {
            const content = this._createContentElement(this._getContentAtIndex(tableRow, index));
            element.appendChild(content);
        });

        return element;
    }

    /**
     * @param {string} content 
     * @return {HTMLElement}
     */
    _createContentElement(content)
    {
        const element = document.createElement(DETAIL_CONTAINER_ELEMENT);
        element.innerHTML = content;
        return element;
    }

    /**
     * @param {HTMLTableRowElement} tableRow 
     * @return {HTMLElement}
     */ 
    _createTitleElement(tableRow)
    {
        return this._createContentElement(this._getContentTitle(tableRow));
    }

    /**
     * @param {HTMLTableRowElement} tableRow 
     */
    _getContentTitle(tableRow)
    {
        return tableRow.querySelector(`td:nth-child(${this._titleColumnIndex})`).innerHTML;
    }

    /**
     * @param {HTMLTableRowElement} tableRow 
     * @param {number} index 
     */
    _getContentAtIndex(tableRow, index)
    {
        return tableRow.querySelector(`td:nth-child(${index})`).innerHTML;
    }

}