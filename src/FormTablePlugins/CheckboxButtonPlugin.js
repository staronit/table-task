import Plugin from './Plugin';

export default class CheckboxButtonPlugin extends Plugin
{
    /**
     * @param {HTMLButtonElement} button 
     * @param {NodeList<HTMLInputElement>} checkboxes 
     */
    constructor(button, checkboxes)
    {
        super();
        this._button = button;
        this._checkboxes = checkboxes;    
    }

    init()
    {
        this._disableButton();
        this._bindCheckboxes();
    }

    _bindCheckboxes()
    {
        this._checkboxes.forEach((checkboxElement) => this._bindCheckbox(checkboxElement));
    }

    /**
     * @param {HTMLInputElement} checkboxElement 
     */
    _bindCheckbox(checkboxElement)
    {
        checkboxElement.addEventListener('change', (event) => this._onCheckboxClick(event.target));
    }

    /**
     * @param {HTMLInputElement} checkboxElement 
     */
    _onCheckboxClick(checkboxElement)
    {
        if (checkboxElement.checked) {
            this._enableButton();
            return;
        }
       
        if (!this._hasSelectedAny()) {
            this._disableButton();
        }
    }
    
    _hasSelectedAny()
    {
        for (let checkbox of this._checkboxes) {
            if (checkbox.checked) {
                return true;
            }
        }

        return false;
    }

    _enableButton()
    {
        this._button.removeAttribute('disabled');
    }

    _disableButton()
    {
        this._button.setAttribute('disabled', "1");
    }



}