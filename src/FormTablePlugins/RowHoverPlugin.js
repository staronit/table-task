import Plugin from './Plugin';
const DEFAULT_ACTIVE_CLASS = 'active';

export default class RowHoverPlugin extends Plugin
{
    /**
     * 
     * @param {NodeList<HTMLInputElement>} checkboxes 
     * @param {function(HTMLInputElement)} rowSelectorFunction 
     * @param {string} hoverClass 
     */
    constructor(checkboxes, rowSelectorFunction, hoverClass = DEFAULT_ACTIVE_CLASS)
    {
        super();
        this._checkboxes = checkboxes;
        this._rowSelectorFunction = rowSelectorFunction;
        this._hoverClass = hoverClass;
    }

    init()
    {
        this._bindCheckboxes();
    }

    _bindCheckboxes()
    {
        this._checkboxes.forEach((checkboxElement) => this._bindCheckbox(checkboxElement));
    }

    /**
     * @param {HTMLInputElement} checkboxElement 
     */
    _bindCheckbox(checkboxElement)
    {
        checkboxElement.addEventListener('change', (event) => this._onCheckboxClick(event.target));
    }

     /**
     * @param {HTMLInputElement} checkboxElement 
     */
    _onCheckboxClick(checkboxElement)
    {
        if (checkboxElement.checked) {
            this._highlight(checkboxElement);
            return;
        }

        this._diminish(checkboxElement);
    }

    /**
     * @param {HTMLInputElement} checkboxElement 
     * @return {HTMLTableRowElement}
     */
    _getRowElement(checkboxElement)
    {
        return this._rowSelectorFunction(checkboxElement);
    }

    _highlight(checkboxElement)
    {
        this._getRowElement(checkboxElement).classList.add(this._hoverClass);
    }

    _diminish(checkboxElement)
    {
        this._getRowElement(checkboxElement).classList.remove(this._hoverClass);
    }



}