var gulp = require('gulp');
var fs = require("fs");
var browserify = require("browserify");
var babelify = require("babelify");
var source = require('vinyl-source-stream');
var addsrc = require('gulp-add-src');
 
gulp.task('es6', function() {
	browserify({ debug: true })
		.transform(babelify)
		.require("./src/main.js", { entry: true })
		.bundle()
		.pipe(source('app.js'))
    .pipe(addsrc.prepend('node_modules/babel-polyfill/dist/polyfill.min.js'))
    .pipe(gulp.dest('./dist'));
});
 
gulp.task('watch',function() {
  gulp.watch(['./src/**/*.js'],['es6'])
});
 
gulp.task('default', ['es6','watch']);